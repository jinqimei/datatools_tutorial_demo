import streamlit as st
import pandas as pd
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier

# Set up the app
st.title("Iris Flower Classification App")
st.write("This app uses a random forest classifier to predict the class of an iris flower based on its sepal and petal measurements.")

# Load the Iris dataset
iris = datasets.load_iris()
X = pd.DataFrame(iris.data, columns=iris.feature_names)
y = pd.Series(iris.target, name='class')

# Train a random forest classifier
clf = RandomForestClassifier()
clf.fit(X, y)

# Define the input widgets
st.write("## Input Values")
sepal_length = st.slider("Sepal length", float(X["sepal length (cm)"].min()), float(X["sepal length (cm)"].max()), float(X["sepal length (cm)"].mean()), key="sepal_length_slider")
sepal_width = st.slider("Sepal width", float(X["sepal width (cm)"].min()), float(X["sepal width (cm)"].max()), float(X["sepal width (cm)"].mean()), key="sepal_width_slider")
petal_length = st.slider("Petal length", float(X["petal length (cm)"].min()), float(X["petal length (cm)"].max()), float(X["petal length (cm)"].mean()), key="petal_length_slider")
petal_width = st.slider("Petal width", float(X["petal width (cm)"].min()), float(X["petal width (cm)"].max()), float(X["petal width (cm)"].mean()), key="petal_width_slider")

# Define the function to make a prediction
def predict(sepal_length, sepal_width, petal_length, petal_width):
    prediction = clf.predict([[sepal_length, sepal_width, petal_length, petal_width]])[0]
    proba_df = pd.DataFrame(clf.predict_proba([[sepal_length, sepal_width, petal_length, petal_width]]), columns=iris.target_names)
    return prediction, proba_df

# Define the button to get the prediction
if st.button("Get Prediction"):
    # Make the prediction
    prediction, proba_df = predict(sepal_length, sepal_width, petal_length, petal_width)

    # Display the prediction and the classifier's probability estimates for each class
    st.write("## Prediction")
    st.write(f"The predicted class is: {iris.target_names[prediction]}")
    st.write("## Class Probabilities")
    st.write(proba_df)
